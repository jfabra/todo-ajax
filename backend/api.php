<?php

/**
 * Este archivo hace en enrutador para la API
 */

// Contiene las rutas a las que se quieren acceder y sus respectivas funciones
$routes = [
    'listas' => ['methods' => ['GET', 'POST', 'PUT', 'DELETE'], 'class' => Listas::class],
    'tareas' => ['methods' => ['GET', 'POST', 'PUT', 'DELETE'], 'class' => Tareas::class],
];

// Contiene las traducciones de los métodos a ejecutar
$execute = [
    'GET'    => 'read',
    'POST'   => 'create',
    'PUT'    => 'update',
    'DELETE' => 'delete',
];

// Se recupera la ruta API a la que se quiere acceder
$api_route = isset($_GET['api_route']) ? $_GET['api_route'] : '';

// Se recupera el método HTTP de la solicitud
$method = $_SERVER['REQUEST_METHOD'];

try {
    // Se recorren las rutas hasta que se encuentra la coincidencia
    $currentRoute = null;
    // Si existe la ruta, se utiliza
    if (isset($routes[$api_route])) {
        $currentRoute = $routes[$api_route];
        
        // Si el método no está permitido se devuelve un sin permisos
        if (!in_array($method, $currentRoute['methods'])) {
            abort('Esta ruta no admite ese tipo de peticiones', 401);
        }

        // Se realiza la llamada antes de mutar la BDD
        prepareBDD();

        // Se ejecuta el método correspondiente
        $instance = $currentRoute['class']::getInstance();
        $result = $instance->execute($execute[$method], getBodyParams($method));
        response([
            'msg'  => 'Se ha ejecutado con éxito',
            'data' => $result,
        ]);
    }
    
    // Si no se ha encontrado la ruta, se devuelve un valor por defecto
    if (is_null($currentRoute)) {
        abort('No se ha podido encontrar la ruta solicitada', 404);
    }
} catch (\StopExecutionException $th) { // Si ha sido controlada
    response(['msg' => $th->getMessage()], $th->getCode());
} catch (\Throwable $th) { // Si no ha sido controlada
    response(['msg' => $th->getMessage()], $th->getCode());
}

// Por defecto se devuelve siempre un JSON
header('Content-type: application/json');

/**
 * Recupera los valores pasados por parámetro
 * 
 * @param method El método ejecutado
 * 
 * @param array
 */
function getBodyParams($method)
{
    // Se incializa a vacío
    $result = [];

    // Se recuperan los valores como corresponda el método
    switch ($method) {
        case 'PUT':
            parse_str(file_get_contents("php://input"), $result);
            break;
        case 'POST':
            $result = array_merge($result, $_POST);
            break;
    }

    // Se añaden siempre los valores del GET
    $result = array_merge($result, $_GET);
    
    return $result;
}

/**
 * Se ejecuta siempre antes de una llamada a la BDD
 * 
 * @return bool
 */
function prepareBDD()
{
    return DbHelper::getInstance()->execute(
        file_get_contents(DB_PATH . '/script.sql')
    );
}

/**
 * Devuelve un mensaje a la API
 * 
 * @param msg  El mensaje que se devolverá
 * @param code El código de respuesta, por defecto 200
 * 
 * @return void
 */
function response($msg, $code = 200)
{
    echo json_encode([
        'data'   => $msg ,
        'status' => $code,
    ]);

    // Se devuelve el mensaje de respuesta
    http_response_code($code);
}

/**
 * Devuelve un error a la API
 * 
 * @param msg  El mensaje que se devolverá
 * @param code El código de respuesta, por defecto 404
 * 
 * @return void
 * @throws StopExecutionException
 */
function abort($msg, $code = 404)
{
    throw new StopExecutionException($msg, $code);
}