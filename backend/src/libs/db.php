<?php

class DbHelper extends PDO
{
    use Singleton;

    /**
     * Contiene la clase de la BDD
     * 
     * @var self
     */
    public $conn;

    public function __construct() {
        try {
            $conn = new parent("mysql:host=" . DB_HOST . ";dbname=" . DB_SCHEMA, DB_USER, DB_PASS);

            // Se configura la conexión
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch(PDOException $e) {
            throw new Exception("No se ha podido establecer la conexión a la Base de Datos", 500);
        }

        $this->conn = $conn;
    }

    public function execute(string $sql)
    {
        $script = $this->conn->prepare($sql);

        return $script->execute();
    }

    /**
     * Añade los condicionales del where al query
     * 
     * @param query La consulta a parsear
     * 
     * @return string
     */
    public function where(string $query, ?array $where)
    {
        // Si se han pasado condiciones se aplican
        if (!is_null($where) && !empty($where)) {
            $query .= " WHERE " . join(' AND ', $where);
        }

        return $query;
    }

    /**
     * Crea contenido
     * 
     * @param table  La tabla de la que eliminar
     * @param params Los parámetros de la operación
     * 
     * @return array
     */
    public function insert($table, $params)
    {
        $fields = $params['fields'];
        $cols = join(', ', array_keys($fields));
        $values = join(', ', array_map(function ($value)
        {
            return ":$value";
        }, array_keys($fields)));

        $query = "INSERT INTO " . $table . " (" . $cols . ") " . "VALUES(" . $values . ")";
        $statement = $this->conn->prepare($query);

        $result = $statement->execute($fields);
        return $result === true ? $this->conn->lastInsertId() : $result;
    }

    /**
     * Consulta contenido
     * 
     * @param table  La tabla de la que eliminar
     * @param params Los parámetros de la operación, por defecto, array vacío
     * @param id     El ID a eliminar, por defecto ninguno
     * 
     * @return array
     */
    public function select($table, $params, $id = '')
    {
        $fields = isset($params['select']) ? $params['select'] : '*';

        // Si se ha pasado un ID se aplica
        $this->addIdToWhere($id, $where);

        $query = "SELECT " . $fields . " FROM " . $table;

        $query = $this->where($query, $where);

        $statement = $this->conn->query($query);
        $result = $statement->fetchAll(PDO::FETCH_OBJ);

        // Si no se encuentra resultados se devuelve 404
        if (!empty($id) && empty($result)) {
            throw new Exception("No se han encontrado resultados", 404);
        }

        return empty($id) ? $result : array_shift($result);
    }

    /**
     * Modifica contenido
     * 
     * @param table  La tabla de la que eliminar
     * @param params Los parámetros de la operación, por defecto, array vacío
     * @param id     El ID a eliminar, por defecto ninguno
     * 
     * @return bool
     */
    public function update($table, $params, $id = '')
    {
        $fields = $params['fields'];

        $update = $fields;
        array_walk($update, function(&$v, $k) {
            $v = "`$k` = '$v'";
        });

        // Si se ha pasado un ID se aplica
        $this->addIdToWhere($id, $where);

        $query = "UPDATE " . $table . " SET " . join(', ', $update);

        $query = $this->where($query, $where);

        $statement = $this->conn->prepare($query);
        $result = $statement->execute($fields);

        return $result;
    }

    /**
     * Borra una o varias filas de la tabla
     * 
     * @param table La tabla de la que eliminar
     * @param id    El ID a eliminar, por defecto ninguno
     * 
     * @return bool
     */
    public function delete(string $table, $id = '')
    {
        // Si se ha pasado un ID se aplica
        $this->addIdToWhere($id, $where);

        $query = "DELETE FROM " . $table;

        $query = $this->where($query, $where);

        return $this->execute($query);
    }

    /**
     * Añade el ID si se encuentra al where
     * 
     * @param id    El ID a evaluar
     * @param where El array con los condicionales
     * 
     * @return void
     */
    public function addIdToWhere(?string $id, ?array &$where)
    {
        // Si se ha pasado un ID se aplica
        if (isset($id) && !empty($id)) {
            $where[] = "id = '$id'";
        }
    }
}