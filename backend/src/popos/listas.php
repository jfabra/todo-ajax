<?php

/**
 * Controla las operaciones CRUD sobre la tabla Listas
 */
class Listas extends Popo
{
    use Singleton;

    /**
     * Contiene el nombre de la tabla
     * 
     * @var string
     */
    public $table = 'listas';

    /**
     * Genera la instancia de la clase Listas
     * 
     * @return self
     */
    public function __construct() {
        parent::__construct($this->table);
    }
}
