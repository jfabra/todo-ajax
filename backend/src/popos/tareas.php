<?php

/**
 * Controla las operaciones CRUD sobre la tabla Tareas
 */
class Tareas extends Popo
{
    use Singleton;

    /**
     * Contiene el nombre de la tabla
     * 
     * @var string
     */
    public $table = 'tareas';

    /**
     * Genera la instancia de la clase Tareas
     * 
     * @return self
     */
    public function __construct() {
        parent::__construct($this->table);
    }
}
