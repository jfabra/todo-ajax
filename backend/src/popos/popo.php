<?php

/**
 * Clase que contiene las operaciones básicas para una tabla de la BDD
 * **P**lain **O**ld **P**HP **O**bject
 */
class Popo implements CRUD
{
    use Singleton;

    /**
     * Contiene el nombre de la tabla
     * 
     * @var string
     */
    public $table;

    /**
     * Instancia de la clase DbHelper
     * 
     * @var DbHelper
     */
    public $db;

    /**
     * Genera la instancia del Popo
     * 
     * @return self
     */
    public function __construct(string $table = '') {
        if (is_null($this->table) && !empty($table)) {
            $this->table = $table;
        }

        $this->db = DbHelper::getInstance();
    }

    /**
     * Crea contenido
     * 
     * @param params Los parámetros de la operación
     * 
     * @return array
     */
    public function create($params)
    {
        $result = [];

        $result = $this->db->insert($this->table, $params);

        return $result;
    }

    /**
     * Modifica contenido
     * 
     * @param params Los parámetros de la operación, por defecto, array vacío
     * 
     * @return array
     */
    public function read($params = [])
    {
        $result = [];
        
        $result = $this->db->select($this->table, $params, $params['id']);

        return $result;
    }

    /**
     * Modifica contenido
     * 
     * @param params Los parámetros de la operación
     * 
     * @return bool
     */
    public function update($params)
    {
        $result = true;

        $result = $this->db->update($this->table, $params, $params['id']);

        return $result;
    }

    /**
     * Modifica contenido
     * 
     * @param params Los parámetros de la operación
     * 
     * @return bool
     */
    public function delete($params)
    {
        $result = true;

        $result = $this->db->delete($this->table, $params['id']);

        return $result;
    }

    /**
     * Se ejecuta el método solicitado con los parámetros recibidos
     * 
     * @param method El método a ejecutar
     * @param params Los parámetros a ejecutar
     * 
     * @return mixed
     * @throws Exception
     */
    public function execute($method, $params)
    {
        // Si no se encuentra el método no se ejecuta
        if (!method_exists($this, $method)) {
            throw new Exception("No se ha encontrado el método a ejecutar", 500);
        }

        // Se ejecuta la llamada al método correspondiente
        return $this->{$method}($params);
    }
}