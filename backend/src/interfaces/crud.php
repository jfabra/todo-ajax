<?php

/**
 * Exige la implementación de las operaciones CRUD (Create, Read, Update, Delete)
 */
interface CRUD
{
    /**
     * Crea contenido
     * 
     * @param params Los parámetros de la operación
     * 
     * @return array
     */
    public function create($params);

    /**
     * Consulta contenido
     * 
     * @param params Los parámetros de la operación
     * 
     * @return array
     */
    public function read($params);

    /**
     * Modifica contenido
     * 
     * @param params Los parámetros de la operación
     * 
     * @return bool
     */
    public function update($params);

    /**
     * Borra contenido
     * 
     * @param params Los parámetros de la operación
     * 
     * @return bool
     */
    public function delete($params);
}