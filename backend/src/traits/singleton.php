<?php

/**
 * Implementa el paradigma singleton
 */
trait Singleton
{
    /**
     * Contiene la instancia de la clase
     * 
     * @var self
     */
    public static $instance;

    /**
     * Devuelve la instancia de la clase
     * 
     * @return self
     */
    public static function getInstance()
    {
        // Si no se ha instanciado todavía, se instancia
        if (is_null(self::$instance)) {
            self::$instance = new self;
        }

        return self::$instance;
    }
}
