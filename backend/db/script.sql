-- ---------------------------------- --
-- Crea la Base de datos del proyecto --
-- ---------------------------------- --
Create Database If Not Exists `todo_ajax`
Default Character Set utf8
Collate utf8_spanish_ci;

Use `todo_ajax`;

-- ---------- --
-- Depuración --
-- ---------- --
/* Drop Table If Exists `tareas`;
Drop Table If Exists `listas`; */

-- ----------------------- --
-- Crea la Tabla de Listas --
-- ----------------------- --
Create Table If Not Exists `listas` (
    -- COLUMNAS
    `id`    Int Not Null auto_increment,
    `title` Varchar(50),
    -- PRIMARY KEY
    Primary Key (`id`)
);

-- ----------------------- --
-- Crea la Tabla de Tareas --
-- ----------------------- --
Create Table If Not Exists `tareas` (
    -- COLUMNAS
    `id`       Int Not Null auto_increment,
    `title`    Varchar(50),
    `status`   Boolean,
    `id_lista` Int,
    -- PRIMARY KEY
    Primary Key (`id`),
    -- FOREIGN KEYS
    Constraint `fk_tareas_lista` Foreign Key (`id_lista`) References `listas`(`id`)
    On Delete Cascade
    On Update Cascade
);
