<?php

/**
 * Este archivo hace de enrutador
 */

require_once __DIR__      . '/../Paths.php'        ;
require_once BACKEND_PATH . '/config/db.inc.php'   ;
require_once SRC_PATH     . '/interfaces/crud.php' ;
require_once SRC_PATH     . '/traits/singleton.php';
require_once POPOS_PATH   . '/popo.php'            ;
require_once POPOS_PATH   . '/listas.php'          ;
require_once POPOS_PATH   . '/tareas.php'          ;
require_once SRC_PATH     . '/libs/db.php'         ;

/**
 * Excepción usada para parar la ejecución
 */
class StopExecutionException extends Exception
{ }

// Contiene las rutas a las que se quieren acceder y sus respectivas funciones
$routes = [
    'api' => ['methods' => ['GET', 'POST', 'PUT', 'DELETE'], 'file' => BACKEND_PATH . '/api.php' ],
];

// Se recupera la ruta API a la que se quiere acceder
$route = isset($_GET['route']) ? $_GET['route'] : '';

// Se recupera el método HTTP de la solicitud
$method = $_SERVER['REQUEST_METHOD'];

try {
    // Se recorren las rutas hasta que se encuentra la coincidencia
    $currentRoute = null;
    // Si existe la ruta, se utiliza
    if (isset($routes[$route])) {
        $currentRoute = $routes[$route];
        
        // Si el método no está permitido se devuelve un sin permisos
        if (!in_array($method, $currentRoute['methods'])) {
            new StopExecutionException('Esta ruta no admite ese tipo de peticiones', 401);
        }

        // Se importa el fichero solicitado
        require_once $currentRoute['file'];
    }
    
    // Si no se ha encontrado la ruta, se devuelve un valor por defecto
    if (is_null($currentRoute)) {
        new StopExecutionException('No se ha podido encontrar la ruta solicitada', 404);
    }
} catch (\StopExecutionException $th) { // Si ha sido controlada
    echo $th->getMessage();
    http_response_code($th->getCode());
} catch (\Throwable $th) { // Si no ha sido controlada
    echo $th->getMessage();
    http_response_code($th->getCode());
}