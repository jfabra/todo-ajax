/**
 * Determina si un valor es un número o no
 * 
 * @param {*} value 
 * 
 * @returns boolean
 */
function isNumeric(value) {
    return /^-?\d+$/.test(value);
}

// El estilo visual en la consola
const LogStyle = 'font-weight: bold; font-size: 12px';