let listas = [];
let tareas = [];

// Al terminar de cargar la página
$(window).on('load', function (evt)
{
    getLists(whenListsLoaded);
    $("#list-create-btn").on('click', function (evt) {
        let input = $(this).prev();
        let value = input.val();

        // Si no se ha pasado contenido, no se crea
        if (value.length == 0) {
            alert('No se ha introducido contenido');
            return;
        }

        insertList({
            'title'   : value,
        }, function (res) {
            let success = res?.data?.data;

            if (success === true) { // Si se ha podido crear se recarga la página
                window.location.reload();
                input.val('');
            } else if (isNumeric(success)) {
                getList(success);
            } else { // Si no, se notifica al usuario
                alert('Lo sentimos, no se ha podido crear la tarea');
            }
        });
    })
});

/**
 * Se ejecuta cuando las listas terminan de cargarse
 * 
 * @returns void
 */
function whenListsLoaded() {
    getTasks();
}