// El nombre del endpoint de listas
const listasEndpoint = 'listas';

/**
 * Devuelve todas las listas
 * 
 * @param {*} callback La llamada
 * 
 * @returns void
 */
 function getLists(callback = null) {
    ajaxGetAll(listasEndpoint, loadList, callback);
}

/**
 * Devuelve todas las listas
 * 
 * @param {*} listId El ID de la lista
 * 
 * @returns void
 */
 function getList(listId) {
    ajaxGet(listasEndpoint, listId, loadList);
}

/**
 * Crea una lista
 * 
 * @param {*} fields   Los campos a crear
 * @param {*} callback El callback al crear la lista
 * 
 * @return void
 */
 function insertList(fields, callback = null) {
    ajaxInsert(listasEndpoint, fields, callback);
}

/**
 * Modifica una lista
 * 
 * @param {*} listId   El ID de la lista
 * @param {*} fields   Los campos a crear
 * @param {*} callback El callback al crear la lista
 * 
 * @return void
 */
 function updateList(listId, fields, callback = null) {
    ajaxUpdate(listasEndpoint, listId, fields, callback);
}

/**
 * Elimina una lista
 * 
 * @param {*} listId   El ID de la lista
 * @param {*} callback El callback al eliminar la lista
 * 
 * @return void
 */
function deleteList(listId, callback = null) {
    ajaxDelete(listasEndpoint, listId, callback);
}

/**
 * Carga toda la información de una lista
 * 
 * @param {*} list Toda la información de una lista
 * 
 * @returns element
 */
function loadList(list) {
    if (dev_mode) console.log('Se genera la lista', list);

    // Se recupera la template y se cambian los datos necesarios
    var listTemplate = document.querySelector('#list-template').content.cloneNode(true);

    $(listTemplate).find('.list-container').attr('id', `list-${list?.id}`);
    $(listTemplate).find('.list-title').html(list?.title);
    $(listTemplate).find('.list-title').on('blur', function () {
        let newtitle = this.innerText.trim();
        updateList(list?.id, {
            'title': newtitle
        });
    });
    $(listTemplate).find('#add-task').on('click', function (evt) {
        let input = $(this).parents('.input-group').find(':text');
        let value = input.val();

        // Si no se ha pasado contenido, no se crea
        if (value.length == 0) {
            alert('No se ha introducido contenido');
            return;
        }

        insertTask({
            'title'   : value,
            'id_lista': list?.id,
        }, function (res) {
            let success = res?.data?.data;

            if (success === true) { // Si se ha podido crear se recarga la página
                window.location.reload();
                input.val('');
            } else if (isNumeric(success)) {
                getTask(success);
            } else { // Si no, se notifica al usuario
                alert('Lo sentimos, no se ha podido crear la tarea');
            }
        });
    });
    $(listTemplate).find('.list-delete').on('click', function (evt) {
        // Se recupera la tarea
        let listElement = $(this).parents('.list-container');
        
        // Se elimina la tarea
        deleteList(list?.id, function (res) {
            let success = res?.data?.data;

            if (success === true) { // Si se ha podido se elimina
                listElement.remove();
            } else { // Si no, se notifica al usuario
                alert('Lo sentimos, no se ha podido eliminar la lista');
            }
        });
    });

    $(".list-create-container").before($(listTemplate));
}