/**
 * Devuelve todas las tuplas
 * 
 * @param {*} endpoint El endpoint solicitado
 * @param {*} loadFunc La función para cargar el elemento solicitado
 * @param {*} callback El callback al crear la tupla
 * 
 * @returns void
 */
 function ajaxGetAll(endpoint, loadFunc = null, callback = null) {
    defaultAjaxCall({
        url:    `${base_route}/api/${endpoint}`,
        method: "GET",
        data: {

        },
        success: function (res) {
            if (dev_mode) console.log(`%c [GET]`, LogStyle, `all ${endpoint}`, res);

            // Si se ha pasado función para renderizar, se renderiza
            if (loadFunc != null) {
                res?.data?.data.forEach(elemento => {
                    loadFunc(elemento);
                });
            }

            // Si se ha pasado callback se ejecuta
            if (callback !== null) {
                callback(res);
            }
        },
    });
}

/**
 * Devuelve una única tupla
 * 
 * @param {*} endpoint El endpoint solicitado
 * @param {*} id       El ID del elemento
 * @param {*} loadFunc La función para cargar el elemento solicitado
 * @param {*} callback El callback al crear la tupla
 * 
 * @returns void
 */
 function ajaxGet(endpoint, id, loadFunc = null, callback = null) {
    defaultAjaxCall({
        url:    `${base_route}/api/${endpoint}/${id}`,
        method: "GET",
        data: {

        },
        success: function (res) {
            if (dev_mode) console.log(`%c [GET]`, LogStyle, `${endpoint}`, id, res);

            // Si se ha pasado función para renderizar, se renderiza
            if (loadFunc != null) {
                loadFunc(res?.data?.data);
            }

            // Si se ha pasado callback se ejecuta
            if (callback !== null) {
                callback(res);
            }
        },
    });
}

/**
 * Crea una tupla
 * 
 * @param {*} endpoint El endpoint solicitado
 * @param {*} fields   Los campos a crear
 * @param {*} callback El callback al crear la tupla
 * 
 * @return void
 */
 function ajaxInsert(endpoint, fields, callback = null) {
    defaultAjaxCall({
        url:    `${base_route}/api/${endpoint}`,
        method: "POST",
        data: {
            "fields": fields,
        },
        success: function (res) {
            if (dev_mode) console.log(`%c [POST]`, LogStyle, `${endpoint}`, fields, res);

            // Si se ha pasado callback se ejecuta
            if (callback !== null) {
                callback(res);
            }
        },
    });
}

/**
 * Modifica una tupla
 * 
 * @param {*} endpoint El endpoint solicitado
 * @param {*} id       El ID de la tupla
 * @param {*} fields   Los campos a modificar
 * @param {*} callback El callback al modificar la tupla
 * 
 * @return void
 */
function ajaxUpdate(endpoint, id, fields, callback = null) {
    defaultAjaxCall({
        url:    `${base_route}/api/${endpoint}/${id}`,
        method: "PUT",
        data: {
            "fields": fields,
        },
        success: function (res) {
            if (dev_mode) console.log(`%c [PUT]`, LogStyle, `${endpoint}`, id, res);

            // Si se ha pasado callback se ejecuta
            if (callback !== null) {
                callback(res);
            }
        },
    });
}

/**
 * Elimina una tupla
 * 
 * @param {*} endpoint El endpoint solicitado
 * @param {*} id       El ID de la tupla
 * @param {*} callback El callback al eliminar la tupla
 * 
 * @return void
 */
function ajaxDelete(endpoint, id, callback = null) {
    defaultAjaxCall({
        url:    `${base_route}/api/${endpoint}/${id}`,
        method: "DELETE",
        data: {

        },
        success: function (res) {
            if (dev_mode) console.log(`%c [DELETE]`, LogStyle, `${endpoint}`, id, res);

            // Si se ha pasado callback se ejecuta
            if (callback !== null) {
                callback(res);
            }
        },
    });
}

/**
 * Realiza una llamada AJAX con jQuery
 * 
 * @param {*} params Los parámetros de la llamada
 * 
 * @returns void
 */
function defaultAjaxCall(params) {
    // Si no existe error, se añade el por defecto
    if (!('error' in params)) {
        params['error'] = error;
    }

    $.ajax(params);
}

/**
 * Muestra un error por consola de una llamada AJAX
 * 
 * @param {*} result 
 * 
 * @returns void
 */
 function error(result) {
    console.error('Se ha encontrado un error, este ha sido el body: ', result);
}