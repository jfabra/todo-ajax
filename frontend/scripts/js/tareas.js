// El nombre del endpoint
const tareasEndpoint = 'tareas';

/**
 * Devuelve todas las tareas
 * 
 * @param {*} callback La llamada
 * 
 * @returns void
 */
 function getTasks(callback = null) {
    ajaxGetAll(tareasEndpoint, loadTask, callback);
}

/**
 * Devuelve todas las tareas
 * 
 * @param {*} taskId El ID de la tarea
 * 
 * @returns void
 */
 function getTask(taskId) {
    ajaxGet(tareasEndpoint, taskId, loadTask);
}

/**
 * Elimina una tarea
 * 
 * @param {*} taskId   El ID de la tarea
 * @param {*} callback El callback al eliminar la tarea
 * 
 * @return void
 */
function deleteTask(taskId, callback = null) {
    ajaxDelete(tareasEndpoint, taskId, callback);
}

/**
 * Modifica una tarea
 * 
 * @param {*} taskId   El ID de la tarea
 * @param {*} fields   Los campos a modificar
 * @param {*} callback El callback al modificar la tarea
 * 
 * @return void
 */
function putTask(taskId, fields, callback = null) {
    ajaxUpdate(tareasEndpoint, taskId, fields, callback);
}

/**
 * Crea una tarea
 * 
 * @param {*} fields   Los campos a crear
 * @param {*} callback El callback al crear la tarea
 * 
 * @return void
 */
function insertTask(fields, callback = null) {
    ajaxInsert(tareasEndpoint, fields, callback);
}

/**
 * Carga toda la información de una tarea
 * 
 * @param {*} task Toda la información de una tarea
 * 
 * @returns element
 */
 function loadTask(task) {
    if (dev_mode) console.log('Se genera la tarea', task);

    // Se recupera la template y se cambian los datos necesarios
    var taskTemplate = document.querySelector('#task-template').content.cloneNode(true);

    $(taskTemplate).find('.task-container').attr('id', `task-${task?.id}`);
    $(taskTemplate).find('.task-title').html(task?.title);
    $(taskTemplate).find('.task-title').on('blur', function () {
        let newtitle = this.innerText.trim();
        putTask(task?.id, {
            'title': newtitle
        });
    });
    $(taskTemplate).find('.task-action.task-status :checkbox').prop('checked', task?.status != null && task?.status == 1);
    $(taskTemplate).find('.task-action.task-delete').on('click', function (evt) {
        // Se recupera la tarea
        let taskElement = $(this).parents('.task-container');
        
        // Se elimina la tarea
        deleteTask(task?.id, function (res) {
            let success = res?.data?.data;

            if (success === true) { // Si se ha podido se elimina
                taskElement.remove();
            } else { // Si no, se notifica al usuario
                alert('Lo sentimos, no se ha podido eliminar la tarea');
            }
        });
    });

    $(taskTemplate).find('.task-action.task-status :checkbox').prop('id', `task-status-checkbox-${task?.id}`);
    $(taskTemplate).find('.task-action.task-status label').prop('for', `task-status-checkbox-${task?.id}`);

    $(taskTemplate).find('.task-container .custom-control.custom-checkbox').on('click', function (evt) {
        let checkbox = $(this).find(':checkbox');

        changeTaskStatus(evt, checkbox, task?.id);
    });

    $(`#list-${task?.id_lista}`).find('.list-items-container').append($(taskTemplate));
}

function changeTaskStatus(evt, checkbox, taskId) {
    // Se recupera la tarea
    let newStatus = !checkbox.prop('checked');

    evt.preventDefault();

    // Se elimina la tarea
    putTask(taskId, {
        'status': newStatus ? 1 : 0,
    }, function (res) {
        let success = res?.data?.data;

        if (success === true) { // Si se ha podido se elimina
            checkbox.prop('checked', newStatus)
        } else { // Si no, se notifica al usuario
            alert('Lo sentimos, no se ha podido actualizar la tarea');
        }
    });
}