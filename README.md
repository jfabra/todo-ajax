# ToDo - AJAX

Este ejercicio va sobre **AJAX** (**A**synchronous **J**avaScript And **X**ML), básicamente, permite ejecutar código **PHP** desde **Javascript** haciendo llamadas.

<https://api.jquery.com/jquery.ajax/>
<https://www.w3schools.com/jquery/jquery_ajax_intro.asp>

## Requisitos
### Aplicación
La aplicación será una lista de tareas (To Do list):
* Ha de poder crear/modificar/borrar listas.
* Ha de poder crear/modificar/borrar tareas.
* Ha de poder marcar como hecho o no una tarea.
* NO tiene que moverse una tarea entre listas como en Trello.
### Back, PHP
La parte de **PHP** realizará consultas a la **BDD** y devolverá los resultados, se recomienda el uso de JSON (**json_encode** para devolver información y **json_decode** pare recibirla).
* Ha de recibir información, distinguir con un parámetro **GET** a que ruta se quiere acceder.
* Ha de validar la información que se pase (opcional, pero recomendable).
* Ha de procesar llamadas a la **BDD** con la información se pase.
* Ha de siempre devolver información, sea un false, un array vacío, lo que sea
### Front

#### HTML
Para el **HTML** se ha de crear la estructura preferida para que los elementos estén repartidos de una manera clara.

#### CSS
Sería una oportunidad bastante buena para usar la librería de **Bootstrap**, y puesto que son elementos sencillos y que se repetirán, acostumbrarse a trabajar con dicha librería y modificar solamente lo necesario.
<https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css>
<https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js>
<https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js>
NO tiene por qué ser responsive (que se pueda ver bien en tabletas y/o móviles).

#### JS
Para **Javascript** se usará la librería de **jQuery**:
<https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js>
En **Javascript** se tiene que:
* Realizar llamadas **AJAX** a los ficheros **PHP** y procesar la información que devuelvan.
* Crear elementos **HTML** con las respuestas que se den.
* Modificar elementos **HTML** con las respuestas que se den.
* Controlar con eventos la interacción del usuario.

#### Consejos
* Para limitar el ancho de una lista/tarjeta se puede usar: **max-width: 100px;**
* Si hay alguna regla que no funciona, **!important** hace que tenga mayor prioridad.
* Se puede usar $.ajax para **GET/POST/PUT/DELETE**, hay ejemplos en internet.
* Para identificar las rutas de **PHP** se puede hacer un único **switch** que filtre las rutas y tenga un default en caso de que no se encuentre dicha ruta.
* Se pueden trabajar con inputs fuera de un formulario.
* Se pueden usar **CDN**s para no tener que descargar las librerías
* Se puede hacer una carga inicial con **$(window).on(‘load’, function (evt) { })**