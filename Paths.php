<?php

/**
 * Este fichero contiene las rutas del sistema
 */

define('ROOT_PATH'     , __DIR__                   );
define('BACKEND_PATH'  , ROOT_PATH    . '/backend' );
define('DB_PATH'       , BACKEND_PATH . '/db'      );
define('SRC_PATH'      , BACKEND_PATH . '/src'     );
define('POPOS_PATH'    , SRC_PATH     . '/popos'   );
define('FRONTEND_PATH' , ROOT_PATH    . '/frontend');